#!/bin/bash
#    scripts for inhomog package
# 
#    Copyright (C) 2013 Jan Ostrowski, Boud Roukema
# 
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2, or (at your option)
#    any later version.
# 
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
# 
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software Foundation,
#    Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
# 
#    See also http://www.gnu.org/licenses/gpl.html
#    

# These scripts are for the fluent shell user.
# graph assumes that the GNU plotutils package is installed.

TMP=`mktemp`
## -w "2 0.05"
./inhomog -t i  > ${TMP}
awk '{print $2,$3}' ${TMP} > ${TMP}.invariants
echo "" >> ${TMP}.invariants
awk '{print $2,(100*$6)}' ${TMP} >> ${TMP}.invariants
echo "" >> ${TMP}.invariants
awk '{print $2,($12)}' ${TMP} >> ${TMP}.invariants # absolute value


#Y_LIMITS='-y "1e-6" 0.025'
Y_LIMITS1="-y"
Y_LIMITS2="1e-7 0.1"
#Y_LIMITS=

for FORMAT in X ps; do
    cat ${TMP}.invariants |
        graph -T${FORMAT} -ly -lx -x 0.02 5.0 ${Y_LIMITS1} ${Y_LIMITS2} \
        -X "R (at z=201)" -Y "\*s\sbI\eb(R), 100\*s\sbII\eb(R), 3|\*s\sbIII\eb(R)|\sp1/3\ep" > ${TMP}.${FORMAT}
done

echo "Files created:"
ls -l ${TMP}*


