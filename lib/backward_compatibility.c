/*
   inhomog backward compatibility routine(s)

   Copyright (C) 2016-2017 Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html

*/

/* Warning: avoid using this: it is deprecated! */

#include <stdio.h>
#include <strings.h>
#include <sys/types.h>
#include <stdint.h>
#include "config.h"
#include "lib/inhomog.h"

void omega_d_precalc_(/* INPUTS */
                      int64_t *scal_av_n_gridsize1,
                      double **I_inv,
                      double **II_inv,
                      double **III_inv,
                      /* updated version needs:
                         aD_norm: proportional to initial volumes^(1/3) */
                      int64_t *n_t_inhomog,
                      double *a_FLRW_init,
                      double *H1bg,
                      /* OUTPUTS */
                      double **t_RZA,
                      double **a_D_RZA
                      ){
  /* INTERNAL */
  double *aD_norm;
  uint64_t n_subdomains, i;

  n_subdomains = (uint64_t)( (*scal_av_n_gridsize1)*
                             (*scal_av_n_gridsize1)*
                             (*scal_av_n_gridsize1) );

  if(NULL== (aD_norm = malloc((size_t)n_subdomains*sizeof(double))) ){
    printf("Deprecated omega_d_precalc_ compatibility routine: ");
    printf("  Error: could not allocate memory.\n");
    exit(1);
  };

  for(i=0; i< n_subdomains; i++){
    aD_norm[i] = 1.0; /* assume all initial scale factors (cube roots
                         of volumes) are equal */
  };

  Omega_D_precalc(/* INPUTS */
                  scal_av_n_gridsize1,
                  scal_av_n_gridsize1,
                  scal_av_n_gridsize1,
                  I_inv,
                  II_inv,
                  III_inv,
                  /* aD_norm: proportional to initial volumes^(1/3) */
                  &aD_norm,
                  n_t_inhomog,
                  a_FLRW_init,
                  H1bg,
                  /* OUTPUTS */
                  t_RZA,
                  a_D_RZA
                  );
}
