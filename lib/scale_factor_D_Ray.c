/*
   scale_factor_D_Ray - based on (9) of Buchert et al RZA2 arXiv:1303.6193v2

   Copyright (C) 2013 Boud Roukema, Jan Ostrowski

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html

*/

/*! \file scale_factor_D_Ray.c */

#include <stdio.h>
#include <sys/types.h>
#include "config.h"
#include <math.h>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_statistics.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv.h>

/* for malloc_usable_size if available */
#ifdef __GNUC__
#include <malloc.h>
#endif

#include "lib/inhomog.h"

#define DEBUG 1

#undef DEBUG


/* isolate Q and/or Lambda effect in integrating ODE */
/*
DISABLE_Q disables adding Q in the integration of the ODE;
DISABLE_LAMBDA_IN_ODE disables adding Lambda in the integration of the
ODE (without modifying other roles of Lambda such as in the
FLRW a(t), \dot{a}(t) relations).
*/

/*! \brief Stores first derivatives for the ODE system routine.
 *
 * First of two necessary functions for the ODE system. Creates a
 * \a func_first_order_deriv array, which stores entries necessary for
 * later procedures (depending on \a t, \f$ a_D \f$ and \a params).
 * Calculation uses Raychaudhuri approach \latexonly (see:
 * \href{https://arxiv.org/abs/1303.6193}{arXiv: 1303.6193} )
 * \endlatexonly .
 *
 * Initially a void parameter, \a params later becomes a structure of
 * a_D_integrator_params_s type.
 *
 * The function also uses GSL cubic spline interpolation libraries to
 * calculate \a Q_D value (kinematical backreaction) needed for the
 * second \a func_first_order_deriv entry.
 *
 * If successful, returns \b GSL_SUCCESS.
 *
 * \param [in] t time parameter (also used for the spline interpolation of Q_D)
 * \param [in] func_first_order an array storing a_D and its derivative [?]
 * \param [in] params a pointer to the void parameter
 * \param [out] func_first_order_deriv an array storing derivatives for
 * the ODE routine
 */
int scale_factor_D_Ray_ODE_func(double t,
                            const double func_first_order[],
                            double func_first_order_deriv[],
                            void *params){
  /*   double mu = *(double *)params; */
  struct a_D_integrator_params_s a_D_integrator_params =
    *(struct a_D_integrator_params_s *)params;
  const double one_third = 1.0/3.0;
  /* const double two_thirds = 2.0/3.0; */

  func_first_order_deriv[0] = func_first_order[1];
  /*  func_first_order_deriv[1] = -func_first_order[0] + mu * func_first_order[1] *
      (1.0 - func_first_order[0]*func_first_order[0]); */
  func_first_order_deriv[1] =
    one_third * (
                 gsl_spline_eval(a_D_integrator_params.spline_Q_D, /* Q_D */
                                 t,
                                 a_D_integrator_params.accel_Q_D) *
                 func_first_order[0] -                      /* a_D */
                 a_D_integrator_params.Aconst /
                 pow(fmin(TOL_RAY_FIRST_ORDER_MAX,
                       fmax(TOL_RAY_FIRST_ORDER_MIN,
                            func_first_order[0])),2.0)
                 );
  return GSL_SUCCESS;
}

/*! \brief Stores the vector of the derivative elements and the Jacobian
 * matrix.
 *
 * Second of two necessary functions for the ODE system. Stores the
 * Jacobian matrix in \a dfunc1d_partials and the time derivatives
 * vector in \a dfunc1D_dt.
 *
 * Initially a void parameter, \a params later becomes a structure of
 * a_D_integrator_params_s type.
 *
 * The Jacobian matrix is of the form:
 * \f$ \begin{bmatrix}
 * \frac{\partial{\dot{x}}}{\partial{x}} &
 * \frac{\partial{\dot{x}}}{\partial{\dot{x}}} \\[5pt]
 * \frac{\partial{\ddot{x}}}{\partial{x}} &
 * \frac{\partial{\ddot{x}}}{\partial{\dot{{x}}}}
 * \end{bmatrix}
 * \f$
 * and the time derivatives vector:
 * \f$ \big( \frac{\partial{\dot{x}}}{\partial{t}},
 *           \frac{\partial{\ddot{x}}}{\partial{t}} \big) \f$ .
 *
 * If successful, returns \b GSL_SUCCESS.
 *
 * \param [in] t time parameter
 * \param [in] func_first_order an array storing a_D and its derivative [?]
 * \param [in] params a pointer to the void parameter
 * \param [out] dfunc1D_partials Jacobian matrix
 * \param [out] dfunc1D_dt vector of the derivative elements
 */
int scale_factor_D_Ray_ODE_jacob(double t,
                             const double func_first_order[],
                             double *dfunc1D_partials,
                             double dfunc1D_dt[],
                             void *params){
  /*  double mu = *(double *)params; */
  struct a_D_integrator_params_s a_D_integrator_params =
    *(struct a_D_integrator_params_s *)params;
  const double one_third = 1.0/3.0;
  const double two_thirds = 2.0/3.0;
  gsl_matrix_view dfunc1D_partials_mat =
    gsl_matrix_view_array(dfunc1D_partials,2,2);
  gsl_matrix * m = &dfunc1D_partials_mat.matrix;
  /* 0 = \dot{original x}; 1 = \dot{\dot{x}} */
  gsl_matrix_set (m, 0, 0, 0.0); /* \partial\dot{x}/\partial x */
  gsl_matrix_set (m, 0, 1, 1.0); /* \partial\dot{x}/\partial \dot{x} */

  /* \partial\dot{\dot{x}}/\partial x : */
  gsl_matrix_set (m, 1, 0,
                  one_third *
                  gsl_spline_eval(a_D_integrator_params.spline_Q_D, /* Q_D */
                                  t,
                                  a_D_integrator_params.accel_Q_D) +
                  two_thirds *
                  a_D_integrator_params.Aconst /
                  (func_first_order[0]*func_first_order[0]*func_first_order[0])  /* a_D^3 */
                  );

  /* \partial\dot{\dot{x}}/\partial \dot{x} : */
  gsl_matrix_set (m, 1, 1, 0.0);

  /* \partial\dot{x}/\partial t */
  dfunc1D_dt[0] = 0.0;
  /* \partial\dot{\dot{x}}/\partial t */
  /* dfunc1D_dt[1] = 0.0; */
  dfunc1D_dt[1] = one_third *func_first_order[0] *
    gsl_spline_eval_deriv(a_D_integrator_params.spline_Q_D, /* Q_D */
                          t,
                          a_D_integrator_params.accel_Q_D);

  return GSL_SUCCESS;
}

/*! \brief Calculates the scale factor \f$ a_D \f$ according to the
 * Raychaudhuri evolution.
 *
 * Uses the Runge-Kutta-Fehlberg (4, 5) integration method.
 *
 * Allows disabling of \f$ Q_D \f$ and/or \f$ \Lambda \f$ during the
 * calculation by using \b DISABLE_Q and \b DISABLE_LAMBDA_IN_ODE macros
 * respectively.
 *
 * The initial time is set to one of two possible values; calculation
 * happens according to either t_EdS or t_flatFLRW function (both are
 * described in more detail in FLRW_background.c) and is controlled by
 * the t_EdS and t_flatFLRW parameters (for a more detailed description
 * see biscale_partition.c). An error message is shown is neither of
 * them is set to 1.
 *
 * Uses kinematical_backreaction function to calculate \f$ Q_D \f$. For
 * a more detailed description see kinematical_backreaction.c .
 *
 * Allows for precalculated I and II invariants through the
 * rza_integrand_params_s control parameter
 * \a precalculated_invariants.enabled. If it's not set, calculates the
 * invariants.
 *
 * To print out invariant calculation errors set \a want_verbose to 1.
 *
 * \param [in] rza_integrand_params_s structure containing parameters
 * necessary for the ODE integration
 * \param [in] background_cosm_params_s structure containing all
 * relevant cosmological parameters (defined outside this file)
 * \param [in] t_background_in pointer to the time values matrix
 * \param [in] n_t_background_in number of entries in the
 * \a t_background_in matrix
 * \param [in] n_sigma[3] an array with maximal numbers of standard
 * deviations for each invariant
 * \param [in] n_calls_invariants integration parameter for functions
 * sigma_sq_invariant_I and sigma_sq_invariant_II
 * \param [in] want_planar control parameter; defined in
 * biscale_partition.c
 * \param [in] want_spherical control parameter; defined in
 * biscale_partition.c
 * \param [in] want_verbose control parameter; defined in
 * biscale_partition.c
 * \param [out] a_D pointer to the scale factor
 * \param [out] dot_a_D pointer to the first derivative of a_D
 * \param [out] unphysical control parameter for checking if values are
 * physically reasonable
 */
int scale_factor_D_Ray(/* INPUTS: */
                   struct rza_integrand_params_s *rza_integrand_params,
                   struct background_cosm_params_s background_cosm_params,
                   double *t_background_in,
                   int  n_t_background_in,
                   double n_sigma[3],
                   long   n_calls_invariants,  /* for invariant I integration */
                   int want_planar, /* cf RZA2 V.A */
                   int want_spherical, /* cf RZA2 V.B.3 */
                   int want_verbose,
                   /* OUTPUTS: */
                   double *a_D,
                   double *dot_a_D,
                   int *unphysical
                   ){

  double *rza_Q_D;
  double *rza_Q_D_plus_Lambda_FLRW;
#define N_T_SPLINE 100000
  /* use external functions to calculate initial t, a, adot */
  double t_initial, a_initial, a_dot_initial;
  double Omm_initial;  /* calculate from Omm_0 for non-EdS case */
  double t_0,dt; /* locally calculate initial and final times */
  double t_background[N_T_SPLINE];

#ifndef DISABLE_LAMBDA_IN_ODE
  double Lambda_invGyr2; /* derived parameter: Lambda in units of Gyr^{-2}; to be calculated from OmLam_0 and H_0 */
#endif

  /* needed for Hamiltonian initial conditions */
#ifdef INHOM_A_D_DOT_INITIAL_HAM
  const double two_thirds = 2.0/3.0;
  const double one_sixth = 1.0/6.0;
  double q_growth_initial;
  double xidot_initial, xiddot_initial, H_initial;
  double curv_initial, kin_backreaction_initial;
#endif

  int i_t;

  /* structure of constant and spline tables for integration */
  struct a_D_integrator_params_s a_D_integrator_params;
  double inv_I, inv_I_err;
  double inv_II, inv_II_err;
  /*     double inv_III, inv_III_err; */

  const gsl_odeiv_step_type *T_gsl_ODE_type =
    gsl_odeiv_step_rkf45;

  /* parameters specific to ODE integrator */
  double t_ODE;
  double h_ODE; /* step size */
  double func_first_order[2]; /* IC = initial conditions */

  gsl_odeiv_system sys_ODE;

  /* declare Q_D spline */
  gsl_interp_accel *accel_Q_D = gsl_interp_accel_alloc();
  gsl_spline *spline_Q_D =
    gsl_spline_alloc(gsl_interp_cspline, N_T_SPLINE);

  gsl_odeiv_step *s_ODE;
  gsl_odeiv_control *c_ODE;
  gsl_odeiv_evolve *e_ODE;

  sys_ODE.function = scale_factor_D_Ray_ODE_func;
  sys_ODE.jacobian = scale_factor_D_Ray_ODE_jacob;
  sys_ODE.dimension = 2;
  sys_ODE.params = &a_D_integrator_params;


  /* prepare for initialising Q_D estimate */
  rza_Q_D = malloc(N_T_SPLINE * sizeof(double));
  rza_Q_D_plus_Lambda_FLRW = malloc(N_T_SPLINE * sizeof(double));

  /* initial time; check that input times are later */
  if(1 == background_cosm_params.EdS){
    t_initial = t_EdS(&background_cosm_params,
                      background_cosm_params.inhomog_a_scale_factor_initial,
                      want_verbose);
  }else if(1 == background_cosm_params.flatFLRW &&
           background_cosm_params.Omm_0 < 1.0){
    t_initial = t_flatFLRW(&background_cosm_params,
                           background_cosm_params.inhomog_a_scale_factor_initial,
                           want_verbose);
  }else{
      printf("No other options for background_cosm_params so far in program.\n");
      return 1;
  };

  if( gsl_stats_min(t_background_in, 1, (size_t)n_t_background_in)
      < t_initial ){
    printf("scale_factor_D_Ray ERROR: smallest t_background_in = %g = too small < %g\n",
           gsl_stats_min(t_background_in, 1, (size_t)n_t_background_in),
           t_initial);
    exit(1);
  };

  /* high number of times needed for calculating spline */
  t_0 = gsl_stats_max(t_background_in, 1, (size_t)n_t_background_in);
  dt= (t_0-t_initial)/(double)(N_T_SPLINE-1);
  for(i_t=0; i_t<N_T_SPLINE-1; i_t++){
    t_background[i_t] = t_initial + (double)i_t *dt;
  };
  t_background[N_T_SPLINE-1] = t_0;

  /* initialise Q_D estimate */
  kinematical_backreaction(rza_integrand_params,
                           background_cosm_params,
                           t_background, N_T_SPLINE,
                           n_sigma,
                           n_calls_invariants,
                           want_planar, /* cf RZA2 V.A */
                           want_spherical,
                           want_verbose,
                           rza_Q_D
                           );

  if(1 == background_cosm_params.EdS){
    for(i_t=0; i_t<N_T_SPLINE; i_t++){
      rza_Q_D_plus_Lambda_FLRW[i_t] = 0.0;

#ifndef DISABLE_Q
      rza_Q_D_plus_Lambda_FLRW[i_t] += rza_Q_D[i_t];
#endif
    };
  }else if(1 == background_cosm_params.flatFLRW &&
           background_cosm_params.Omm_0 < 1.0){
#ifndef DISABLE_LAMBDA_IN_ODE
    Lambda_invGyr2 =
      3.0 * background_cosm_params.OmLam_0 *
      background_cosm_params.H_0 * COSM_H_0_INV_GYR *
      background_cosm_params.H_0 * COSM_H_0_INV_GYR ;
#endif
    for(i_t=0; i_t<N_T_SPLINE; i_t++){
      rza_Q_D_plus_Lambda_FLRW[i_t] = 0.0;

#ifndef DISABLE_Q
      rza_Q_D_plus_Lambda_FLRW[i_t] += rza_Q_D[i_t];
#endif

#ifndef DISABLE_LAMBDA_IN_ODE
      rza_Q_D_plus_Lambda_FLRW[i_t] += Lambda_invGyr2;
#endif
    };
  }; /*   if(1 == background_cosm_params.EdS) */


  /* initialise Q_D spline */
  gsl_spline_init( spline_Q_D,
                   t_background,
                   rza_Q_D_plus_Lambda_FLRW, /* rza_Q_D, */
                   N_T_SPLINE );

  /* TODO: fix programming risk - the programmer might set
     rza_integrand_params.background_cosm_params
     at a higher level and not expect it to be modified here */
  rza_integrand_params->background_cosm_params =
    background_cosm_params; /* needed by the invariant integrators for P(k) */

  if(rza_integrand_params->precalculated_invariants.enabled){
    inv_I = rza_integrand_params->precalculated_invariants.inv_I;
    inv_II = rza_integrand_params->precalculated_invariants.inv_II;
  }else /* (re-)calculate inv_I, inv_II if needed */ {

    if(rza_integrand_params->sigma_sq_inv_triple.I_known){
      /* set the scale of inv_I */
      inv_I = n_sigma[0] *
        rza_integrand_params->sigma_sq_inv_triple.sqrt_E_sigma_sq_I;
    }else{
      sigma_sq_invariant_I( *rza_integrand_params,
                            n_calls_invariants,
                            want_verbose,
                            &inv_I, &inv_I_err );
      /* set the scale of inv_I */
      rza_integrand_params->sigma_sq_inv_triple.sqrt_E_sigma_sq_I =
        sqrt(inv_I);
      rza_integrand_params->sigma_sq_inv_triple.I_known = 1;
      inv_I = n_sigma[0] *
        rza_integrand_params->sigma_sq_inv_triple.sqrt_E_sigma_sq_I;
    };

    if(rza_integrand_params->sigma_sq_inv_triple.II_known){
      /* set the scale of inv_II */
      inv_II = n_sigma[1] *
        rza_integrand_params->sigma_sq_inv_triple.sqrt_E_sigma_sq_II;
    }else{
      sigma_sq_invariant_II( *rza_integrand_params,
                             3*n_calls_invariants,
                             want_verbose,
                             &inv_II, &inv_II_err );

    /* set the scale of inv_II */
      rza_integrand_params->sigma_sq_inv_triple.sqrt_E_sigma_sq_II =
        sqrt(inv_II);
      rza_integrand_params->sigma_sq_inv_triple.II_known = 1;
      inv_II = n_sigma[1] *
        rza_integrand_params->sigma_sq_inv_triple.sqrt_E_sigma_sq_II;
    };
  };


  if(want_verbose &&
     !rza_integrand_params->precalculated_invariants.enabled
     ){
    printf("scale_factor_D_Ray (pre-integration): inv_I err = %g %g\n",
           inv_I,  fabs(n_sigma[0]) * 0.5*inv_I_err/inv_I);
    printf("scale_factor_D_Ray (pre-integration): inv_II err = %g %g\n",
           inv_II,  fabs(n_sigma[1]) * 0.5*inv_II_err/inv_II);
  };




  if(1==background_cosm_params.EdS){
    a_initial =
      a_EdS(&background_cosm_params, t_initial, want_verbose);
    a_dot_initial =
      a_dot_EdS(&background_cosm_params, t_initial, want_verbose);
    a_D_integrator_params.Aconst = 1.5 *
      a_dot_initial*a_dot_initial /
      (a_initial*a_initial) *
      background_cosm_params.inhomog_a_d_scale_factor_initial *
      background_cosm_params.inhomog_a_d_scale_factor_initial *
      background_cosm_params.inhomog_a_d_scale_factor_initial *
      (1.0 - inv_I);
  }else if(1 == background_cosm_params.flatFLRW &&
           background_cosm_params.Omm_0 < 1.0){
    a_initial =
      a_flatFLRW(&background_cosm_params,
                 t_initial, want_verbose);
    a_dot_initial =
      a_dot_flatFLRW(&background_cosm_params,
                     t_initial, want_verbose);
    /* flat FLRW case, e.g. (1.1) of arXiv:1303.4444;
       assumes FLRW flatness!
     */
    Omm_initial = background_cosm_params.Omm_0 /
      ( exp(3.0*log(a_initial/
                    background_cosm_params.inhomog_a_scale_factor_now)) *
        (1.0 - background_cosm_params.Omm_0) +
        background_cosm_params.Omm_0 );

    a_D_integrator_params.Aconst = 1.5 *
      a_dot_initial*a_dot_initial /
      (a_initial*a_initial) *
      background_cosm_params.inhomog_a_d_scale_factor_initial *
      background_cosm_params.inhomog_a_d_scale_factor_initial *
      background_cosm_params.inhomog_a_d_scale_factor_initial *
      Omm_initial *
      (1.0 - inv_I);
  }else{
    printf("scale_factor_D_Ray. Background models other than EdS\n");
    printf("and flat FLRW not yet programmed.\n");
    return 1;
  };
  a_D_integrator_params.accel_Q_D = accel_Q_D;
  a_D_integrator_params.spline_Q_D = spline_Q_D;

#ifdef INHOM_A_D_DOT_INITIAL_HAM
  q_growth_initial =
    growth_FLRW(&background_cosm_params,
                t_initial, want_verbose);
  xidot_initial =
    dot_growth_FLRW(&background_cosm_params,
                    t_initial, want_verbose)/
    q_growth_initial;
  xiddot_initial =
    ddot_growth_FLRW(&background_cosm_params,
                     t_initial, want_verbose)/
    q_growth_initial;

  H_initial = a_dot_initial/a_initial;

  kin_backreaction_initial =
    xidot_initial * xidot_initial*
    (2.0*inv_II - two_thirds*inv_I*inv_I) ;

  curv_initial =
    xidot_initial*xidot_initial*
    (-2.0* inv_II
     -12.0* inv_I * H_initial/xidot_initial
     -4.0* inv_I *xiddot_initial /(xidot_initial*xidot_initial)
     );
#endif /* INHOM_A_D_DOT_INITIAL_HAM */

  /* set up ODE parameters */
  h_ODE = 1e-6 *t_initial;
  /* *  a_D_initial  */
  func_first_order[0] =
    background_cosm_params.inhomog_a_d_scale_factor_initial;

  /* \dot{a_D}_initial */
  if(1==background_cosm_params.EdS ||
     (1 == background_cosm_params.flatFLRW &&
      background_cosm_params.Omm_0 < 1.0)
     ){
    /* RZA2 VI.A: */
#ifdef INHOM_A_D_DOT_INITIAL_BKS
      func_first_order[1] =
      a_dot_initial * (1.0 + inv_I /3.0);
#endif

#ifdef INHOM_A_D_DOT_INITIAL_HAM
    /* Hamiltonian initial condition: */
    func_first_order[1] =
      a_initial *
      sqrt(fmax(TOL_ADOT_INITIAL_SQUARED,
                a_D_integrator_params.Aconst*two_thirds
                /(background_cosm_params.inhomog_a_d_scale_factor_initial *
                  background_cosm_params.inhomog_a_d_scale_factor_initial *
                  background_cosm_params.inhomog_a_d_scale_factor_initial)
                - one_sixth * curv_initial
                * (double)INHOM_A_D_DOT_INITIAL_CURV_FACTOR
                - one_sixth * kin_backreaction_initial
                + (background_cosm_params.OmLam_0 *
                   background_cosm_params.H_0 *
                   background_cosm_params.H_0 *
                   COSM_H_0_INV_GYR*COSM_H_0_INV_GYR)
                ));
#endif /* INHOM_A_D_DOT_INITIAL_HAM */

  }else{
    printf("scale_factor_D_Ray. Other models not yet programmed.\n");
    printf("background_cosm_params.EdS = %d\n",
           background_cosm_params.EdS);
    printf("background_cosm_params.flatFLRW = %d\n",
           background_cosm_params.flatFLRW);
    printf("background_cosm_params.Omm_0 = %g\n",
           background_cosm_params.Omm_0);
    return 1;
  };

  s_ODE = gsl_odeiv_step_alloc(T_gsl_ODE_type,2);
  c_ODE = gsl_odeiv_control_y_new(0.0,1e-6);
  e_ODE = gsl_odeiv_evolve_alloc(2);

  t_ODE = t_initial;
  a_D[0] = func_first_order[0];
  dot_a_D[0] = func_first_order[1];

  /* solve the ODE */
  for (i_t=0; i_t<n_t_background_in; i_t++){
    /* Assume first step is physically reasonable,
       and initially assume any subsequent step
       to have the same step as the step preceding
       it.
     */
    if(0 == i_t){
      unphysical[i_t] = 0;
    }else{
      unphysical[i_t] = unphysical[i_t-1];
    };

    if(0<i_t &&
       /* check for physically reasonable values */
       fabs(dot_a_D[i_t-1] *
            (t_background_in[i_t] -
             t_background_in[i_t-1])) >
       MAX_DEL_A_D_ACCEPTABLE *
       background_cosm_params.inhomog_a_scale_factor_now){
      unphysical[i_t] = 1;
    };
    if(!unphysical[i_t]){
      while (t_ODE < t_background_in[i_t] &&
             !unphysical[i_t]
             ){
        gsl_odeiv_evolve_apply(e_ODE, c_ODE, s_ODE,
                               &sys_ODE,
                               &t_ODE,
                               t_background_in[i_t],
                               &h_ODE,
                               func_first_order);
        a_D[i_t] = func_first_order[0];
        dot_a_D[i_t] = func_first_order[1];
        /* check for physically reasonable values */
        if( fabs(a_D[i_t]) >
            MAX_DEL_A_D_ACCEPTABLE *
            background_cosm_params.inhomog_a_scale_factor_now ||
            fabs(dot_a_D[i_t-1] *
                 (t_background_in[i_t] -
                  t_background_in[i_t-1])) >
            MAX_DEL_A_D_ACCEPTABLE *
            background_cosm_params.inhomog_a_scale_factor_now){
          unphysical[i_t] = 1;
        };
      }; /* while (t_ODE < t_background_in[i_t]) */
    }else{
      a_D[i_t] = -9e9; /* TODO - virial value here? */
      dot_a_D[i_t] = -9e19; /* TODO - or signal undefined? */
    }; /* if(!unphysical[i_t]) */
  }; /* for (i_t=0; i_t<n_t_background_in; i_t++) */


  /* trivial test: test rza_Q_D  at original input times */
  /*
  for(i_t=0; i_t<n_t_background_in; i_t++){
    a_D[i_t] = gsl_spline_eval(spline_Q_D,
                               t_background_in[i_t],
                               accel_Q_D);
  };
  */

  gsl_odeiv_step_free (s_ODE);
  gsl_odeiv_evolve_free (e_ODE);
  gsl_odeiv_control_free (c_ODE);

  gsl_spline_free(spline_Q_D);
  gsl_interp_accel_free (accel_Q_D);

  free(rza_Q_D);
  free(rza_Q_D_plus_Lambda_FLRW);
  return 0;
}
