/* 
   test_fftw3 - test fftw3 library - see http://fftw.org

   Copyright (C) 2013 Boud Roukema, Jan Ostrowski

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

   See also http://www.gnu.org/licenses/gpl.html
   
*/

#include <stdio.h>
#include <sys/types.h>
#include "config.h"
#include <math.h>
#include <time.h>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_statistics.h>
#include <gsl/gsl_complex.h>
#include <gsl/gsl_complex_math.h>

#include <fftw3.h>

#include "lib/inhomog.h"
#include "lib/power_spectrum_flatspace.h"
#include "lib/test_sigma_invariants.h"


int test_fftw3_delta(void); /* prototype */


/* for learning fftw3 */
int test_fftw3_delta(void){

  const int N_x = TEST_INVAR_N_x; /* N_k_sample := N_x */
  const int N_k_sample = TEST_INVAR_N_x; /* N_k_sample := N_x */
  /*   const double kmax = TEST_INVAR_kmax; */
  fftw_complex *delta, *delta_tilde, *delta_B; 
  fftw_plan plan_A, plan_B;
  
  gsl_complex * delta_gsl;
  /*   gsl_complex delta_tilde_gsl[TEST_INVAR_N_x]; */

  double rr; /* radius for gaussian */
  int i_x,i_y;
  int i_k;
  int i_k_centred_x;
  int i_k_centred_y;
  int i_k_centred;

  double ratio_in_out, count;
  double ratio_in_out_abs;
  double xx;
  double ratio_in_out_sig;
  

  delta = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * 
                                      (unsigned)(N_k_sample*N_k_sample));
  delta_gsl = malloc(sizeof(gsl_complex) * (unsigned)(TEST_INVAR_N_x*TEST_INVAR_N_x));

  delta_tilde = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * 
                                            (unsigned)(N_k_sample*N_k_sample));
  delta_B = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * 
                                        (unsigned)(N_k_sample*N_k_sample));

  plan_A = fftw_plan_dft_2d(N_k_sample, N_k_sample, 
                            delta, 
                            delta_tilde, 
                            FFTW_FORWARD, FFTW_ESTIMATE);
  plan_B = fftw_plan_dft_2d(N_k_sample, N_k_sample, 
                            delta_tilde, 
                            delta_B, 
                            FFTW_BACKWARD, FFTW_ESTIMATE);
  /* initialise an input function - a Gaussian with a centre that is
     offset from the origin;
     the offset should cause a phase effect only, i.e. not visible in the norm
     of the transform */
  for(i_x=0; i_x<N_x; i_x++){
    for(i_y=0; i_y<N_x; i_y++){
      /* offset from x centre at 70% from array beginning */
      rr = (double)(i_x)- 0.7*(double)N_x;
      /* y centre is at centre of y range */
      rr = sqrt(rr*rr + (i_y - 0.5*N_x)*(i_y - 0.5*N_x));
      /* example of assigning to gsl_complex type */
      delta_gsl[i_x*N_x + i_y] = gsl_complex_rect
        (gsl_ran_gaussian_pdf( rr, (double)N_x/2.0 ), 0.0);
      /* example of assigning to fftw_complex type */
      delta[i_x*N_x + i_y][0] = GSL_REAL(delta_gsl[i_x*N_x + i_y]);
      delta[i_x*N_x + i_y][1] = GSL_IMAG(delta_gsl[i_x*N_x + i_y]);
    };
  };
  
  fftw_execute(plan_A); /* do the transform */

  /* normalise */
  /*
  for(i_x=0; i_x<N_x; i_x++){
    for(i_y=0; i_y<N_x; i_y++){
      delta_tilde[i_x*N_x + i_y][0] *= (double)N_x;
      delta_tilde[i_x*N_x + i_y][1] *= (double)N_x;
    };
  };
  */

  /* print example output */
  for(i_k=0+1; i_k<N_k_sample; i_k++){
    i_k_centred_x = (i_k + (N_k_sample/2)) % N_k_sample; /* modulus if arguments > 0 */
    i_k_centred_y = N_k_sample/2;
    i_k_centred = i_k_centred_x*N_x + i_k_centred_y;
    printf("fftw test1: %d %d %d %g\n",
           i_k, i_k_centred_x, i_k_centred_y,
           sqrt(delta_tilde[i_k_centred][0]*delta_tilde[i_k_centred][0] +
                delta_tilde[i_k_centred][1]*delta_tilde[i_k_centred][1]) 
           *N_x
           );
  };

  /* try normalising before the inverse transform */
  printf("Normalising before the inverse transform.\n");
  for(i_x=0; i_x<N_x; i_x++){
    for(i_y=0; i_y<N_x; i_y++){
      delta_tilde[i_x*N_x + i_y][0] /= N_x;
      delta_tilde[i_x*N_x + i_y][1] /= N_x;
    };
  };
  

  fftw_execute(plan_B); /* do the inverse transform */


  ratio_in_out = 0.0;
  ratio_in_out_abs = 0.0;
  ratio_in_out_sig = 0.0;
  count = 0.0;
  for(i_x=0; i_x<N_x; i_x++){
    for(i_y=0; i_y<N_x; i_y++){
      if(fabs(delta[i_x*N_x + i_y][0]) > 1e-14){
        xx = delta_B[i_x*N_x + i_y][0] / delta[i_x*N_x + i_y][0];
        ratio_in_out += xx;
        ratio_in_out_abs += fabs(xx);
        ratio_in_out_sig += xx*xx;
        count += 1.0;
      };
      if(fabs(delta[i_x*N_x + i_y][1]) > 1e-14){
        xx = delta_B[i_x*N_x + i_y][1] / delta[i_x*N_x + i_y][1];
        ratio_in_out += xx;
        ratio_in_out_abs += fabs(xx);
        ratio_in_out_sig += xx*xx;
        count += 1.0;
      };
    };
  };

  ratio_in_out /= count;
  ratio_in_out_abs /= count;
  ratio_in_out_sig = sqrt(ratio_in_out_sig/(count-1.0) - ratio_in_out*ratio_in_out),
  printf("out/in ratio of Re, Im parts: mean meanabs sig = %g %g %g; N_x, N_x^2= %d %d\n",
         ratio_in_out,
         ratio_in_out_abs,
         ratio_in_out_sig, N_x, N_x*N_x
         );

  fftw_destroy_plan(plan_A);
  fftw_destroy_plan(plan_B);
  fftw_free(delta); 
  fftw_free(delta_tilde);
  fftw_free(delta_B);
  fftw_cleanup();

  free(delta_gsl);
  
  
  return 0;
}



int main(void){
  test_fftw3_delta();
  return 0;
}

