/*
   test_Omega_D - averaged Omega parameters - RZA2 arXiv:1303.6193v2

   Copyright (C) 2013 Boud Roukema, Jan Ostrowski

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html

*/

#include <stdio.h>
#include <sys/types.h>
#include "config.h"
#include <math.h>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_statistics.h>

/* for malloc_usable_size if available */
#ifdef __GNUC__
#include <malloc.h>
#endif

#include <time.h>

#include "lib/inhomog.h"

/* int test_Omega_D(
                 struct rza_integrand_params_s rza_integrand_params,
*/
int main(void)
{
  struct rza_integrand_params_s rza_integrand_params;
  struct background_cosm_params_s background_cosm_params;

  int want_verbose = 0;

#define N_PLOT_T 6
#define N_PLOT_R 2
#define N_COSM 2
  double R_domain_mod[N_PLOT_R];

  /*  double t_i = 0.004574998; */ /* for z=200, H_0=50, EdS */
  /* double t_0 = 13.04;       */ /* for z=200, H_0=50, EdS */
  double t_i[N_COSM];
  double t_0[N_COSM];
  double t_background[N_COSM][N_PLOT_T];
  double rza_Q_D[N_COSM][N_PLOT_R][N_PLOT_T];
  double rza_R_D[N_COSM][N_PLOT_R][N_PLOT_T];
  double a_D[N_COSM][N_PLOT_R][N_PLOT_T];
  double dot_a_D[N_COSM][N_PLOT_R][N_PLOT_T];
  double unphysical[N_COSM][N_PLOT_R][N_PLOT_T];
  double H_D[N_COSM][N_PLOT_R][N_PLOT_T];
  double Omm_D[N_COSM][N_PLOT_R][N_PLOT_T];
  double OmQ_D[N_COSM][N_PLOT_R][N_PLOT_T];
  double OmLam_D[N_COSM][N_PLOT_R][N_PLOT_T];
  double OmR_D[N_COSM][N_PLOT_R][N_PLOT_T];
  double delta_t;
  double delta_ln_R;
  double a_FLRW_expected;
  double a_dot_FLRW_expected;
  double H_FLRW_expected;

  /* test mode: compare with RZA2 arXiv:1303.6193v2 */
  int want_planar = 0; /* cf RZA2 V.A */
  int want_spherical = 0; /* cf RZA2 V.B.3 */
  /* TODO: choose pseudo-tophat window function from RZA2 */

  /* TODO: choose power spectrum from RZA2 */


  double Omm0,Omm1,OmR0,OmR1;
  double Omm_D_test_rel_diff[N_PLOT_R] = {0.2, 0.3};
  double OmR_D_test_rel_diff[N_PLOT_R] = {0.1, 0.6};
  int pass = 0;
  int pass_eights = 1;

  const gsl_rng_type * T_gsl;
  gsl_rng * r_gsl;
  static unsigned long int local_gsl_seed=0;

  long   n_calls_invariants = 100000; /* 300000; */
  double n_sigma[3] =
  /*    {2.0, 2.0, 2.0};  */
    {1.0, 1.0, 1.0};
  /*    {-1.0, -1.0, -1.0};   */
  /* {0.0, -1.0, 0.0};  */
  /* {-1.0, -1.0, -1.0};   */
  /* {1.0, 0.0, 1.0};   */
  /*    {-1.0, 0.3333333333333, -0.037037037};  */

  /* values for Figs 2--6 RZA2 */
#define N_RZA2 3
  double n_sigma_RZA2[N_RZA2][3] =
    { {2.0, 2.0, 2.0},
      {1.0, 1.0, 1.0},
      {-1.0, -1.0, -1.0} };
  int i_RZA2; /* iterate over RZA2 figure requirements */
  int rza2_figures=0; /* enable calculation for all RZA2 figures? plotting test */
  int n_rza2; /* either N_RZA2 or an override */

  double R_domain_lower_BKS00;
  double R_domain_upper_BKS00;

  /*  New expected values could be generated with:

     ./lib/test_Omega_D | \
     egrep "13\.(0373|8663)" | \
     awk '{print $16,$19,$20,$21}'

     but do not do that unless you are completely sure
     that you know what you are doing.
  */
  double expected_Omegas[N_COSM][2][4] =    /* n_calls_invariants = 1000000; */
    { { {1.11859, 0.175305, 1.49921e-05, 0.663382},
        {1.03, 0.24028, -7.37023e-05, 0.70989 } },
      { {1.31981, 0.28709, -0.012072, 0},
        {1.03379, 0.846419, -0.00194487, 0} } };
  /* n_calls_invariants = 300000; */
  /*
    { { {1.1178, 0.1758, 0.000103667, 0.663858},
        {1.02825, 0.241843, -6.35389e-05, 0.710868} },
      { {1.31723, 0.290207, -0.0105453, 0},
        {1.06082, 0.744882, -0.004969, 0} } };
  */

  /* n_calls_invariants = 10000;
    { { {1.11369, 0.175526, 3.59408e-05, 0.663589},
        {1.02291, 0.245114, -8.68882e-05, 0.713223} },
      { {1.30339, 0.288883, -0.0113041, 0  },
        {1.05623, 0.737999, -0.00639778, 0  } } };
  */


  /* tolerances for n_calls_invariants = 30000, ~ 70 sec calculation on 2 cores; */
  double Omega_tol[N_COSM][2][4] =
    { {{ 0.07, 0.05, 50.0,  0.99 },
       { 0.02, 0.02, 10.0,   0.99 }},
      {{ 0.07, 0.08, 1.0, 0.99 },
       { 0.01, 0.08, 0.8, 0.99 }} };

  /* TEST_PRECALC is for testing Omega_D with precalculated values of the initial
     invariants. */
  /* #define TEST_PRECALC 1 */
  /* #define BENCHMARK_LCDM 1 */

#ifdef TEST_PRECALC
  double invs_initial[3]={0.01,1e-4,1e-6};
#endif

  /* benchmarking */
  clock_t  benchmark[10]; /* num elements hardwired! */
  int  i_bench=0;

  /* int    I_invariant; */

  int i_t, i_R; /* counters in time and length scale */
  int i_EdS;

  gsl_rng_env_setup();
  T_gsl = gsl_rng_default;
  gsl_rng_default_seed += 2467 + local_gsl_seed;
  local_gsl_seed = gsl_rng_default_seed;
  r_gsl = gsl_rng_alloc (T_gsl); /* this gets reallocated each time the
                                    function gets called */


  /* cf BKS00 Table 1, Fig 1 : final FLRW comoving units  16 Mpc, 100 Mpc */
  /*
    R_domain_lower_BKS00 = 0.08;
    R_domain_upper_BKS00 = 0.5;
  */

  rza_integrand_params.w_type = 1; /* test standard spherical domains */

  rza_integrand_params.delta_R = DELTA_R_DEFAULT; /*   if(2==rza_integrand_params.w_type) */

  /* 'B' = BBKS (BKS version); 'e' = EisHu98 short formula set */
  rza_integrand_params.pow_spec_type = 'e';

#ifndef TEST_PRECALC
  rza_integrand_params.precalculated_invariants.enabled = 0;
  rza_integrand_params.sigma_sq_inv_triple.I_known = 0;
  rza_integrand_params.sigma_sq_inv_triple.II_known = 0;
  rza_integrand_params.sigma_sq_inv_triple.III_known = 0;
#else
  rza_integrand_params.precalculated_invariants.enabled = 1;
  rza_integrand_params.sigma_sq_inv_triple.I_known = invs_initial[0];
  rza_integrand_params.sigma_sq_inv_triple.II_known = invs_initial[1];
  rza_integrand_params.sigma_sq_inv_triple.III_known = invs_initial[2];
#endif


  /* possible overrides: this may make   expected_a_D_Omegas  cause failure */
  /* RZA2  Fig 2 :
     physical radii approx 0.125 Mpc, 0.5 Mpc;
     final FLRW comoving diameters (2R) approx 50 Mpc, 200 Mpc */
  /* R_domain_lower_BKS00 = 0.5 * 20.0*INHOMOG_A_SCALE_FACTOR_INITIAL / INHOMOG_A_SCALE_FACTOR_NOW;  */
  /*   R_domain_lower_BKS00 = 0.5 * 50.0*INHOMOG_A_SCALE_FACTOR_INITIAL / INHOMOG_A_SCALE_FACTOR_NOW;  */
  /* R_domain_lower_BKS00 =  33.8 *INHOMOG_A_SCALE_FACTOR_INITIAL / INHOMOG_A_SCALE_FACTOR_NOW;  */
  /* TODO: use the constants in inhomog.h */
  /* R_domain_lower_BKS00 = 0.5 * 25.0/(1.0+z_initial);  */
  /* R_domain_upper_BKS00 = 0.5 * 100.0*INHOMOG_A_SCALE_FACTOR_INITIAL / INHOMOG_A_SCALE_FACTOR_NOW;   */
  /*   R_domain_upper_BKS00 = 0.5 * 200.0*INHOMOG_A_SCALE_FACTOR_INITIAL / INHOMOG_A_SCALE_FACTOR_NOW;  */

  if(rza2_figures){
    n_rza2 = N_RZA2;
  }else{
    n_rza2 = 1;
  };

  background_cosm_params.flatFLRW = 1;

  background_cosm_params.inhomog_a_scale_factor_initial = 1.0/201.0;
  background_cosm_params.inhomog_a_d_scale_factor_initial = 1.0/201.0;
  background_cosm_params.inhomog_a_scale_factor_now = 1.0;

  background_cosm_params.correct_Pk_norm_known = 0;
  background_cosm_params.correct_Pk_norm = 1.0; /* should be ignored */


  R_domain_lower_BKS00 =
    0.5 * 50.0
    *background_cosm_params.inhomog_a_scale_factor_initial
    / background_cosm_params.inhomog_a_scale_factor_now;
  R_domain_upper_BKS00 =
    0.5 * 200.0
    *background_cosm_params.inhomog_a_scale_factor_initial
    / background_cosm_params.inhomog_a_scale_factor_now;


  for(i_RZA2=0; i_RZA2<n_rza2; i_RZA2++){

    n_sigma[0]= n_sigma_RZA2[i_RZA2][0];
    n_sigma[1]= n_sigma_RZA2[i_RZA2][1];
    n_sigma[2]= n_sigma_RZA2[i_RZA2][2];

    benchmark[i_bench] = clock(); /* initialise timer */

    for(i_EdS=0; i_EdS<N_COSM; i_EdS++){
      background_cosm_params.EdS = i_EdS;
      printf("background_cosm_params.EdS  = %d\n",
             background_cosm_params.EdS);

      i_bench ++;
      benchmark[i_bench]=clock();
      printf("beginning of i_EdS = %d loop\n",i_EdS);
      print_benchmark(benchmark[i_bench-1],benchmark[i_bench]);

      background_cosm_params.recalculate_t_0 = 1; /* initially recalculate for this test */
      background_cosm_params.Theta2p7 = 2.726/2.7; /* e.g. arXiv:0911.1955 */
      if(1 == background_cosm_params.EdS){
        background_cosm_params.H_0 = 50.0;
        background_cosm_params.Omm_0 = 1.0;
        background_cosm_params.OmLam_0 = 0.0;
        background_cosm_params.Ombary_0 = 0.088;
        background_cosm_params.sigma8 = 0.83;

        t_i[i_EdS] = t_EdS(&background_cosm_params,
                           background_cosm_params.inhomog_a_scale_factor_initial,
                           want_verbose);
      }else if(1 == background_cosm_params.flatFLRW){
#ifndef BENCHMARK_LCDM
        background_cosm_params.H_0 = 70.0;
        background_cosm_params.OmLam_0 = 0.73;
        background_cosm_params.Ombary_0 = 0.044;
        background_cosm_params.sigma8 = 0.83;
        /*
          background_cosm_params.H_0 = 50.0;
          background_cosm_params.OmLam_0 = 0.001;
        */
        background_cosm_params.Omm_0 = 1.0 - background_cosm_params.OmLam_0;
#else
        background_cosm_params.H_0 = 50.0;
        background_cosm_params.Omm_0 = 0.999;
        background_cosm_params.OmLam_0 = 0.001;
        background_cosm_params.Ombary_0 = 0.088;
        background_cosm_params.sigma8 = 0.83;
#endif
        t_i[i_EdS] = t_flatFLRW(&background_cosm_params,
                                background_cosm_params.inhomog_a_scale_factor_initial,
                                want_verbose);
      }else{
        printf("No other options for background_cosm_params so far in program.\n");
        exit(1);
      };

      background_cosm_params.recalculate_t_0 = 1;



      printf("\ntest_Omega_D:\n");

      t_0[i_EdS] = background_cosm_params.t_0;
      delta_t = (t_0[i_EdS]-t_i[i_EdS])/((double)(N_PLOT_T-1));
      delta_ln_R = (log(R_domain_upper_BKS00) - log(R_domain_lower_BKS00))/(double)(N_PLOT_R-1);

      /* random (50:50) choice of + or - fluctuations, independently for
         each of the three invariants  */

      /* not used in RZA2 article:
         for(I_invariant=0; I_invariant<3; I_invariant++){
         if(gsl_rng_uniform(r_gsl) > 0.5)
         n_sigma[I_invariant] = -n_sigma[I_invariant];
         };
      */

      for(i_t=0; i_t<N_PLOT_T; i_t++){
        t_background[i_EdS][i_t] = t_i[i_EdS] + (double)i_t * delta_t;
      };

      /* allow nested openmp if possible - so that the invariants can be calculated in parallel */
      /* #ifdef _OPENMP
         omp_set_nested(1);
         #endif
      */

#pragma omp parallel                            \
  default(shared)                               \
  private(i_R)                                  \
  firstprivate(rza_integrand_params,            \
               background_cosm_params)
      {
#pragma omp for schedule(dynamic)
        for(i_R=0; i_R<N_PLOT_R; i_R++){
          switch(rza_integrand_params.w_type)
            {
            case 1:
            default:
              R_domain_mod[i_R] = exp( log(R_domain_lower_BKS00) + ((double)i_R)* delta_ln_R );
              rza_integrand_params.R_domain = R_domain_mod[i_R];
              break;

            case 2:
              R_domain_mod[i_R] = exp( log(R_domain_lower_BKS00) + ((double)i_R)* delta_ln_R );
              rza_integrand_params.R_domain_1 = R_domain_mod[i_R] -
                0.5*rza_integrand_params.delta_R;
              rza_integrand_params.R_domain_2 = R_domain_mod[i_R] +
                0.5*rza_integrand_params.delta_R;
              break;
            };

          rza_integrand_params.sigma_sq_inv_triple.I_known = 0;
          rza_integrand_params.sigma_sq_inv_triple.II_known = 0;
          rza_integrand_params.sigma_sq_inv_triple.III_known = 0;


          Omega_D(&rza_integrand_params,
                  background_cosm_params,
                  (double*)&(t_background[i_EdS][0]), (int)N_PLOT_T,
                  n_sigma,
                  n_calls_invariants,
                  want_planar, /* cf RZA2 V.A */
                  want_spherical,
                  want_verbose,
                  (double*)&(rza_Q_D[i_EdS][i_R][0]),
                  (double*)&(rza_R_D[i_EdS][i_R][0]),
                  (double*)&(a_D[i_EdS][i_R][0]),
                  (double*)&(dot_a_D[i_EdS][i_R][0]),
                  (int*)&(unphysical[i_EdS][i_R][0]),
                  (double*)&(H_D[i_EdS][i_R][0]),
                  (double*)&(Omm_D[i_EdS][i_R][0]),
                  (double*)&(OmQ_D[i_EdS][i_R][0]),
                  (double*)&(OmLam_D[i_EdS][i_R][0]),
                  (double*)&(OmR_D[i_EdS][i_R][0])
                  );
        };  /*    for(i_R=0; i_R<N_PLOT_R; i_R++) */
      }; /* #pragma omp parallel                            */


      for(i_R=0; i_R<N_PLOT_R; i_R++){
        for(i_t=0; i_t<N_PLOT_T; i_t++){
          if(0==i_EdS){
            a_FLRW_expected =
              a_flatFLRW(&background_cosm_params,
                         t_background[i_EdS][i_t],
                         want_verbose);
            a_dot_FLRW_expected =
              a_dot_flatFLRW(&background_cosm_params,
                             t_background[i_EdS][i_t],
                             want_verbose) ;
            H_FLRW_expected = a_dot_FLRW_expected /
              a_FLRW_expected;
          }else{
            a_FLRW_expected = exp(log(t_background[i_EdS][i_t]/t_i[i_EdS])*(2.0/3.0))
              * background_cosm_params.inhomog_a_scale_factor_initial;
            H_FLRW_expected = 2.0/(3.0*t_background[i_EdS][i_t]);
            a_dot_FLRW_expected = H_FLRW_expected * a_FLRW_expected;
          };

          /*
          if(0==i_EdS && 0==i_t && 0==i_R){
            printf("test_scale_factor_D: Warning: curvature backreaction inferred\n");
            printf("from Hamilton constraint; not calculated directly.\n");
          };
          OmR_D[i_EdS][i_R][i_t] =
            1.0  - Omm_D[i_EdS][i_R][i_t]
            - OmQ_D[i_EdS][i_R][i_t]
            - OmLam_D[i_EdS][i_R][i_t];
          */

          printf("nsig=%d%d%d i_EdS=%d R t : a_D/a H_D/H : Omm_D OmQ_D OmL_D OmR_D  %g %g : %g %g : %g %g %g %g\n",
                 (int)(floor(n_sigma[0])*1.000001),
                 (int)(floor(n_sigma[1])*1.000001),
                 (int)(floor(n_sigma[2])*1.000001),
                 i_EdS,
                 R_domain_mod[i_R],
                 t_background[i_EdS][i_t],
                 a_D[i_EdS][i_R][i_t]/a_FLRW_expected,
                 /* dot_a_D[i_EdS][i_R][i_t]/a_dot_FLRW_expected, */
                 exp(2.0*log(H_FLRW_expected/H_D[i_EdS][i_R][i_t])),  /*  (H_FLRW/H_D)^2 */
                 Omm_D[i_EdS][i_R][i_t],
                 OmQ_D[i_EdS][i_R][i_t],
                 OmLam_D[i_EdS][i_R][i_t],
                 OmR_D[i_EdS][i_R][i_t]
                 );
        }; /* for(i_t=0; i_t<N_PLOT_T; i_t++) */
        i_t = N_PLOT_T-1;

        if( !isfinite(a_D[i_EdS][i_R][i_t]) ||
            fabs( a_D[i_EdS][i_R][i_t]/ a_FLRW_expected
                  - expected_Omegas[i_EdS][i_R][0] ) * 0.5 /
            fabs( a_D[i_EdS][i_R][i_t]/ a_FLRW_expected
                  + expected_Omegas[i_EdS][i_R][0] ) > Omega_tol[i_EdS][i_R][0] )
          pass += 1 * pass_eights;
        if( !isfinite(Omm_D[i_EdS][i_R][i_t]) ||
            fabs( Omm_D[i_EdS][i_R][i_t]
                  - expected_Omegas[i_EdS][i_R][1] ) * 0.5 /
            fabs( Omm_D[i_EdS][i_R][i_t]
                  + expected_Omegas[i_EdS][i_R][1] ) > Omega_tol[i_EdS][i_R][1] )
          pass +=  2  * pass_eights;
        if( !isfinite(OmQ_D[i_EdS][i_R][i_t]) ||
            fabs( OmQ_D[i_EdS][i_R][i_t]
                  - expected_Omegas[i_EdS][i_R][2] ) * 0.5 /
            fabs( OmQ_D[i_EdS][i_R][i_t]
                  + expected_Omegas[i_EdS][i_R][2] ) > Omega_tol[i_EdS][i_R][2] )
          pass +=  4  * pass_eights;
        pass_eights *= 8;
        printf("final H_D, R_domain*a_D  = %g %g\n",
               H_D[i_EdS][i_R][N_PLOT_T-1] /COSM_H_0_INV_GYR,
               R_domain_mod[i_R] *a_D[i_EdS][i_R][N_PLOT_T-1]
               );

        printf(":%d:%d:pass = %d;",i_EdS,i_R,pass);

          printf("  errors = %g %g %g \n",
                 fabs( a_D[i_EdS][i_R][i_t]/ a_FLRW_expected
                       - expected_Omegas[i_EdS][i_R][0] ) * 0.5 /
                 fabs( a_D[i_EdS][i_R][i_t]/ a_FLRW_expected
                       + expected_Omegas[i_EdS][i_R][0] ),
                 fabs( Omm_D[i_EdS][i_R][i_t]
                       - expected_Omegas[i_EdS][i_R][1] ) * 0.5 /
                 fabs( Omm_D[i_EdS][i_R][i_t]
                       + expected_Omegas[i_EdS][i_R][1] ),
                 fabs( OmQ_D[i_EdS][i_R][i_t]
                       - expected_Omegas[i_EdS][i_R][2] ) * 0.5 /
                 fabs( OmQ_D[i_EdS][i_R][i_t]
                       + expected_Omegas[i_EdS][i_R][2] ));
      }; /* for(i_R=0; i_R<N_PLOT_R; i_R++) */

      i_bench ++;
      benchmark[i_bench]=clock();
      printf("...this test took: ");
      print_benchmark(benchmark[i_bench-1],benchmark[i_bench]);

    }; /*   for(i_EdS=0; i_EdS<N_COSM; i_EdS++) */

  i_bench ++;
  benchmark[i_bench]=clock();
  printf("...the last few lines took: ");
  print_benchmark(benchmark[i_bench-1],benchmark[i_bench]);

  printf("tcalc_LCDM/tcalc_EdS: %8ld %7.2f \n",
         n_calls_invariants,
         (double)(benchmark[2]-benchmark[1])/
         (double)(benchmark[4]-benchmark[3]));



    for(i_R=0; i_R<N_PLOT_R; i_R++){
      Omm0 = Omm_D[0][i_R][N_PLOT_T-1];
      Omm1 = Omm_D[1][i_R][N_PLOT_T-1];
      OmR0 = OmR_D[0][i_R][N_PLOT_T-1];
      OmR1 = OmR_D[1][i_R][N_PLOT_T-1];
      printf("test_scale_factor:\n");
      if(  fabs((Omm0-Omm1)*2.0/(Omm0+Omm1)) > Omm_D_test_rel_diff[i_R] ){
        /* pass += 64*i_R + 1; */
        printf(" WARNING.\n");
      };
      if(  fabs((OmR0-OmR1)*2.0/(OmR0+OmR1)) > OmR_D_test_rel_diff[i_R] ){
        /* pass += 64*i_R + 2; */
        printf(" WARNING.\n");
      }else{
        printf("\n");
      };
      printf(" final Omm_D, OmR_D EdS vs nearly-EdS at R = %g relative diff = %g %g\n",
             R_domain_mod[i_R],
             fabs((Omm0-Omm1)*2.0/(Omm0+Omm1)),
             fabs((OmR0-OmR1)*2.0/(OmR0+OmR1))
             );
      printf(" Omm_D = %g %g",Omm0, Omm1);
      printf(" OmR_D = %g %g\n",OmR0, OmR1);
    };
  };  /* for(i_RZA2=0; i_RZA2<n_rza2; i_RZA2++) */

  gsl_rng_free(r_gsl);

  printf("test_Omega_D: pass = %d\n",pass);
  return pass;
}
