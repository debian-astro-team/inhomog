/*
   test_parse_noempty_strtox - wrappers for strtod and strtol for command line parsing

   Copyright (C) 2017 Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
   See also http://www.gnu.org/licenses/gpl.html

*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <string.h>
#include <errno.h>
#include "lib/parse_noempty_strtox.h"


/* int test_parse_noempty_strtox(void){ */
int main(void){
  char string[256];
  char test_string[256];
  char *endptr;
  double xx=-5.55;
  long   nn=-77;
  int    i_fatal_fail;
  int j;

  strncpy(string," 32 76.0 47",256);

  strncpy(test_string,"45",256);
  parse_noempty_strtox(test_string, "test_parse_noempty_strtox:","--fortyfive",'l',0,
                  &nn, &endptr);
  printf("nn = %ld\n",nn);

  strncpy(test_string,"-42",256);
  parse_noempty_strtox(test_string, "test_parse_noempty_strtox:","--fortytwo",'l',0,
                  &nn, &endptr);
  printf("nn = %ld\n",nn);

  strncpy(test_string,"",256);
  parse_noempty_strtox(test_string, "test_parse_noempty_strtox:","--empty",'l',0,
                  &nn, &endptr);
  printf("nn = %ld\n\n",nn);

  strncpy(test_string,"  23.0",256);
  parse_noempty_strtox(test_string, "test_parse_noempty_strtox:","--twentythree",'d',0,
                  &xx, &endptr);
  printf("xx = %f\n",xx);

  strncpy(test_string,"-42.0 ",256);
  parse_noempty_strtox(test_string, "test_parse_noempty_strtox:","--minusfortytwo",'d',0,
                  &xx, &endptr);
  printf("xx = %f\n",xx);

  strncpy(test_string,"-42.0 ",256);
  parse_noempty_strtox(test_string, "test_parse_noempty_strtox:","--minusfortytwo invalid type",'q',0,
                  &xx, &endptr);
  printf("xx = %f\n",xx);

  strncpy(test_string,"",256);
  parse_noempty_strtox(test_string, "test_parse_noempty_strtox:","--fortyfive",'d',0,
                  &xx, &endptr);
  printf("xx = %f\n\n",xx);

  printf("string=%s\n",string);
  for(i_fatal_fail=-1; i_fatal_fail<2; i_fatal_fail++){
    printf("i_fatal_fail = %i ... \n",i_fatal_fail);
    endptr = string;
    for(j=0; j < 5; j++){
      printf("j=%d:  ",j);
      parse_noempty_strtox(endptr, "test_parse_noempty_strtox:","--multiparam",
                           'd',i_fatal_fail,
                           &xx, &endptr);
      printf("xx = %f\n\n",xx);
    };
  };


  return 0;
}
